from odoo import models, fields


class StudentLibBook(models.Model):
    _name = 'student.lib.book'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    student_ids = fields.Many2one('student.info', string='Student Names')
    book_ids = fields.Many2many('library.book', string='Books')
    taken_date = fields.Date('Taken Date')

    book_condition = fields.Char('Book Note', track_visibility="always")
    student_note = fields.Char('Student Note', track_visibility="always")

    def action_confirm(self):
        for rec in self:
            rec.state = 'confirm'

    def action_taken(self):
        for rec in self:
            rec.state = 'taken'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('taken', 'Taken'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, default='draft', track_visibility="always")
