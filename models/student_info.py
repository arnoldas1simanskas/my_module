from odoo import models, fields, api
from odoo.exceptions import ValidationError


class StudentInfo(models.Model):
    _name = 'student.info'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Student Information'
    _order = 'last_name asc, name'
    _rec_name = 'full_name'

    name = fields.Char('Name', required=True, track_visibility="always")
    last_name = fields.Char('Last Name', required=True, track_visibility="always")
    full_name = fields.Char(string='Full Name', compute='get_full_name')
    register_date = fields.Datetime(string="Register Date", default=fields.Datetime.now())
    student_age = fields.Integer('Age', track_visibility="always")
    gender = fields.Selection([
        ('male', 'Male'),
        ('fe_male', 'Female'),
    ], default='male', string="Gender", track_visibility="always")

    age_group = fields.Selection([
        ('major', 'Major'),
        ('minor', 'Minor'),
    ], string="Age Group", compute='set_age_group', track_visibility="always")

    teacher = fields.Many2many(
        'res.users',
        string='Teachers',
        track_visibility="always",
    )

    @api.depends('student_age')
    def set_age_group(self):
        for rec in self:
            if rec.student_age:
                if rec.student_age < 18:
                    rec.age_group = 'minor'
                else:
                    rec.age_group = 'major'

    @api.depends('name', 'last_name')
    def get_full_name(self):
        for fn in self:
            fn.full_name = fn.name + ' ' + fn.last_name

    @api.constrains('student_age')
    def check_age(self):
        for rec in self:
            if rec.student_age < 16:
                raise ValidationError('The age must be greater then 15')
