from odoo import models, fields, api


class LibraryBook(models.Model):
    _name = 'library.book' # Table in DB => library_book
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Library Book'
    _order = 'date_release desc, name'
    _rec_name = 'name' # this shows the name not the id, but by default name already shows string not id
    name = fields.Char('Title', required=True)
    date_release = fields.Date('Release Date')
    date_from = fields.Datetime(string="Insert Date", default=fields.Datetime.now())
    author_ids = fields.Many2many(
        'res.partner',
        string='Authors'
    )
    # short_name = fields.Char(
    #      string='Short Title',
    #      size=100,  # For Char only
    #      translate=False,   # also for Text fields
    # )
    notes = fields.Text('Internal Notes')
    description = fields.Html('Description')
    cover = fields.Binary('Book Cover')
    out_of_print = fields.Boolean('Out of Print?')
    date_updated = fields.Datetime('Last Updated')
    pages = fields.Integer(
        string='Number of Pages',
        default=0,
        help='Total book page count',
        groups='base.group_user',
        states={'lost': [('readonly', True)]},
        copy=True,
        index=False,
        readonly=False,
        required=False,
        company_dependent=False,
        )
    reader_rating = fields.Float(
        'Reader Average Rating',
        digits=(14, 4),  # Optional precision (total, decimals),
    )

    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [
            ('draft', 'available'),
            ('available', 'borrowed'),
            ('borrowed', 'available'),
            ('available', 'lost'),
            ('borrowed', 'lost'),
            ('lost', 'available')]
        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        for book in self:
            if book.is_allowed_transition(book.state,
                                          new_state):
                book.state = new_state
            else:
                continue

