{
 'name': "Library Books",
 'summary': "Manage your books",
 'description': """Long description""",
 'author': "Test Name",
 'license': "AGPL-3",
 'website': "http://www.example.com",
 'category': 'Uncategorized',
 'version': '11.0.1.0.0',
 'depends': ['base', 'website', ],
 'data': [
            'security/groups.xml',
            'views/library_book.xml',
            'views/student_info.xml',
            'views/student_lib_book.xml',
            'views/templates.xml',
            'views/book_view.xml',
            'security/ir.model.access.csv',
            'reports/report.xml',
            'reports/student_card.xml',
 ],
 'demo': ['demo.xml'],
 'application': True,
 'auto_install': False,
 'installable': True,
}
