# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
# from odoo.addons.website.controllers.main import Website


class Main(http.Controller):

    @http.route('/my_library/all-books', type='http', auth='none')
    def all_books(self):
        books = request.env['library.book'].sudo().search([])
        html_result = '<html><body><ul>'
        for book in books:
            html_result += "<li> %s </li>" % book.name
        html_result += '</ul></body></html>'
        return html_result

    @http.route('/my_library/all-books/mark-mine', type='http', auth='public')
    def all_books_mark_mine(self):
        books = request.env['library.book'].sudo().search([])
        html_result = '<html><body><ul>'
        for book in books:
            if request.env.user.partner_id.id in book.author_ids.ids:
                html_result += "<li> <b>%s</b> </li>" % book.name
            else:
                html_result += "<li> %s </li>" % book.name
        html_result += '</ul></body></html>'
        return html_result

    @http.route('/my_library/all-books/mine', type='http', auth='user')
    def all_books_mine(self):
        books = request.env['library.book'].search([
            ('author_ids', 'in', request.env.user.partner_id.ids),
        ])
        html_result = '<html><body><ul>'
        for book in books:
            html_result += "<li> %s </li>" % book.name
        html_result += '</ul></body></html>'
        return html_result

    # website i true yra butinas kad galetum normaliai naudoti template komanda
    @http.route('/my_library/test', type='http', auth='public', methods=['GET'], website=True)
    def book_test(self):
        objects = http.request.env['library.book'].sudo().search([])  #sudo() allows to bypass all the access points

        return http.request.render('my_module.book_request', {
            'objects': objects,
        })

    @http.route('/my_library/test/<int:obj_id>', type='http', auth='public', methods=['GET'], website=True)
    def book_view_test(self, obj_id):
        objects = http.request.env['library.book'].sudo().browse(obj_id)

        return http.request.render('my_module.book_view', {
            'objects': objects,
        })
